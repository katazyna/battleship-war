package com.example.katayna.battleshipwar;
import android.graphics.Color;
import android.widget.Button;

public class Ship {

    private int shipSize;

    private Button[] shipButtons; //buvo private

    Ship(int shipSize) {
        this.shipSize = shipSize;
        this.shipButtons = new Button[shipSize];
    }

    public void insertShipButton(int buttonIndex, Button button) {
        this.shipButtons[buttonIndex] = button;
    }

    public Button getShipButton(int buttonIndex)
    {
        return this.shipButtons[buttonIndex];
    }

    public int getShipSize() {
        return this.shipSize;
    }

    public Boolean shipDown() {

        for (int i = 0; i < this.shipButtons.length; i++) { // . . x
            if (shipButtons[i].getText() != "X") {
                break;
            }
            if (i == this.shipButtons.length-1) {
                if (shipButtons[i].getText() == "X") {
                    return true;
                }
            }
        }

        return false;
    }


}
