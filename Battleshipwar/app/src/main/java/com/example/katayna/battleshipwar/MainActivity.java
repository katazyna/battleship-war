package com.example.katayna.battleshipwar;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TextView nameText = (TextView) findViewById(R.id.nameText);
        //if(!(nameText.getText().toString().equals(""))) {

            Button startBtn = (Button) findViewById(R.id.startBtn);
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView nameText = (TextView) findViewById(R.id.nameText);
                    if(!(nameText.getText().toString().equals(""))) {
                        startActivity(new Intent(MainActivity.this, MyOtherActivity.class));
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(!isFinishing()){
                                    new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Alert")
                                        .setMessage("Enter your name")
                                        .setCancelable(false)
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            startActivity(getIntent());
                                            }
                                        }).show();
                                }
                            }
                        });
                    }
                }
            });

        Button scoresBtn = (Button) findViewById(R.id.scoresBtn);
        scoresBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 TextView nameText = (TextView) findViewById(R.id.nameText);
                 startActivity(new Intent(MainActivity.this, MyResultActivity.class));

             }
         });



/*
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   if(!isFinishing()){
                       new AlertDialog.Builder(MainActivity.this)
                       .setTitle("Alert")
                       .setMessage("Enter your name")
                       .setCancelable(false)
                       .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialog, int which) {
                               finish();
                               startActivity(getIntent());
                           }
                       }).show();
                   }
               }
           });
*/


    }
}
